package history

import (
	"errors"
	"time"
)

type History struct {
	Entries    []Entry
	maxHistory int
	runCounter int
}

type Entry struct {
	HistRun   int       `json:"hist_run"`
	RandNum   int       `json:"lucky_num"`
	VisitedAt time.Time `json:"visited_at"`
}

// New function  to initialise and create History slice
func New(maxHistory int) (*History, error) {
	if maxHistory <= 0 {
		return nil, errors.New("maxHistory must be greater than 0")
	}

	history := History{
		maxHistory: maxHistory,
		Entries:    make([]Entry, 0), // "0" to avoid displaying misleading info in incomplete history during initial run, eg., less than 3 records
	}

	return &history, nil
}

// Add function to append history entry https://tour.golang.org/moretypes/15
func (h *History) Add(luckyNum int) {
	t := time.Now()
	h.runCounter++
	h.Entries = append(h.Entries, Entry{
		HistRun:   h.runCounter,
		RandNum:   luckyNum,
		VisitedAt: t,
	})
	if len(h.Entries) == h.maxHistory+1 { // housekeeping on Histroy Entry
		h.Entries = h.Entries[1 : h.maxHistory+1]
	}
}

func (h *History) Len() int {
	return len(h.Entries)
}

func (h *History) RemoveOldestEntry() error {
	if len(h.Entries) == 0 {
		return errors.New("history empty")
	}

	h.Entries = h.Entries[1:len(h.Entries)]
	return nil
}

func (h *History) ClearAll() error {
	if len(h.Entries) == 0 {
		return errors.New("history empty, cannot clear")
	}
	h.Entries = h.Entries[:0]
	return nil
}
