package history_test

import (
	"testing"

	"gitlab.com/shasderias/history"
)

func TestAdd(t *testing.T) {
	if _, err := history.New(-1); err == nil {
		t.Error("history.New(-1) should return an error")
	}

	if _, err := history.New(0); err == nil {
		t.Error("history.New(0) should return an error")
	}

	h, err := history.New(3)
	if err != nil {
		t.Error(err)
	}
	if h.Len() != 0 {
		t.Error("a new history should be empty")
	}

	h.Add(15)
	if h.Len() != 1 {
		t.Error("expected history to contain 1 entry, got", h.Len())
	}
	h.Add(10)
	h.Add(40)
	h.Add(30)
	if h.Len() != 3 {
		t.Error("expected history to contain 3 entries, got", h.Len())
	}
}
